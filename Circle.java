import java.lang.Math;

public class Circle {
       
    int radius;
    Point center;

    public Circle(int r, Point c) {
        radius = r;
        center = c;
    }

    public int area() {
        return radius * radius;
    }
    
    public int perimeter() {
        return 2 * radius;
    }
    
    public boolean intersect(Circle circ) {
        if (Math.sqrt((center.xCoord - circ.center.xCoord)*(center.xCoord - circ.center.xCoord)+(center.yCoord - circ.center.yCoord)*(center.yCoord - circ.center.yCoord)) < radius + circ.radius) return true;
        return false;
    }
}	
