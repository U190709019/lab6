import java.util.Arrays;

public class Rectangle {
    
    int sideA;
    int sideB;
    Point topLeft;
    
    public Rectangle(int a, int b, Point l) {
        sideA = a;
        sideB = b;
        topLeft = l;
    }

    public int area() {
        return sideA * sideB;
    }

    public int perimeter() {
        return 2 * (sideA + sideB);
    }


    public Point[] corners() {
        Point topRight = new Point(topLeft.xCoord+sideA,topLeft.yCoord);
        Point downLeft = new Point(topLeft.xCoord,topLeft.yCoord-sideB);
        Point downRight = new Point(topLeft.xCoord+sideA,topLeft.yCoord-sideB);
        Point[] dots = {topLeft, topRight, downLeft, downRight};
        return dots;
    }

}
