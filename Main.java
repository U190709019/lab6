import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        
        Point p1 = new Point(4,5);
        Rectangle rect1 = new Rectangle(3,5,p1);
    
        System.out.println("Rectangle's area: " + rect1.area());
        System.out.println("Rectangle's perimeter: " + rect1.perimeter());
        Point[] nokta = rect1.corners();
        String[] word = {"Top Left", "Top Right", "Down Left", "Down Right"};
        for (int i = 0; i < nokta.length; i++)
            System.out.println(word[i] + ": (" + nokta[i].xCoord + ", " + nokta[i].yCoord + ")");

        Point p2 = new Point(3,7);
        Point p3 = new Point(400,50); 
        Circle circ1 = new Circle(10, p2);
        Circle circ2 = new Circle(5, p3);
        System.out.println("Circle 1 center: ("+p2.xCoord+", "+p2.yCoord+") Radius: "+ circ1.radius);
        System.out.println("Circle 2 center: ("+p3.xCoord+", "+p3.yCoord+") Radius: "+ circ2.radius);
        System.out.println("Circle 1's area: " + circ1.area()+ "pi");
        System.out.println("Circle 1's perimeter: " + circ1.perimeter() + "pi");
        if (circ1.intersect(circ2)) System.out.println("Circle 1 and Circle 2 are intersect.");
        else System.out.println("Circle 1 and Circle 2 are not intersect");
    }
    

}
